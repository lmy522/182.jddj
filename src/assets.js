import logo from './assets/logo.png'

export default{
    install(Vue){
        Vue.prototype.assets = {
            logo,
        }
    }
}