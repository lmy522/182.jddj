import axios from 'axios';
import qs from 'qs';
axios.defaults.baseURL = 'http://106.12.85.208:8081';
axios.defaults.transformRequest = [
    (data)=>{
        return qs.stringify(data);
    }
];
export default{
    install(Vue){
        Vue.prototype.imgUrl='http://106.12.85.208:8081';
        Vue.prototype.axios = axios;
    }
}