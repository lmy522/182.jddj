import Vue from 'vue'
import App from './App.vue'
import car from "./components/car.vue";//购物车组件
import privileges from "./components/privileges.vue";//优惠券组件
import router from './router'
import assets from './assets';
import global from './global';
import VueCookies from 'vue-cookies';
//全局组件
Vue.component('car', car)
Vue.component('privileges', privileges)

Vue.config.productionTip = true

Vue.use(global);
Vue.use(assets);
Vue.use(VueCookies);

new Vue({
  router,
  data: {
    cars: {}
  },
  render: function (h) { return h(App) }
}).$mount('#app')
