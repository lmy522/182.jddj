import Vue from "vue";
import Router from "vue-router";

import ElementUI from "element-ui";
import VueAwesomeSwiper from 'vue-awesome-swiper'
import "element-ui/lib/theme-chalk/index.css";
import 'swiper/dist/css/swiper.css'

import index from "./components/index.vue";//首页
import register from "./components/register.vue";//登录
import enroll from "./components/enroll.vue";//注册

import category from "./components/category.vue";//全部商品
import details from "./components/details.vue";//商品详情

import discovery from "./components/discovery.vue";//发现
import attention from "./views/attention.vue";//关注
import cookbook from "./views/cookbook.vue";//食谱
import recommend from "./views/recommend.vue";//推荐
import fashion from "./views/fashion.vue";//时尚
import exist from "./views/exist.vue";//生活


import shoppingCart from "./components/shoppingCart.vue";//购物车
import indentsd from "./components/indentsd.vue";//订单
import mine from "./components/mine.vue";//我的


Vue.use(VueAwesomeSwiper)

Vue.use(Router);
Vue.use(ElementUI);

export default new Router({
  routes: [
    {
      path: "/index",
      name: "index", //首页
      component: index,
    },
    {
      path: "/index/category/:shop_id",
      name: "category", //全部商品
      component: category,
      props: true

    },
    {
      path: "/index/details/:shop_id",
      name: "details", //商品详情
      component: details,
      props: true
    },
    {
      path: "/discovery",
      name: "discovery", //发现
      component: discovery,
      redirect: {
        name: "attention"
      },
      children: [
        {
          path: "attention",
          name: 'attention',//关注
          component: attention
        },
        {
          path: "cookbook",
          name: 'cookbook',//食谱
          component: cookbook
        },
        {
          path: "recommend",
          name: 'recommend',//推荐
          component: recommend
        },
        {
          path: "fashion",
          name: 'fashion',//时尚
          component: fashion
        },
        {
          path: "exist",
          name: 'exist',//生活
          component: exist
        },
      ]
    },
    {
      path: "/shoppingCart",
      name: "shoppingCart", //购物车
      component: shoppingCart
    },
    {
      path: "/indentsd",
      name: "indentsd", //订单
      component: indentsd
    },
    {
      path: "/mine",
      name: "mine", //我的
      component: mine
    },
    {
      path: "/register",
      name: "register", //登录
      component: register
    },
    {
      path: "/enroll",
      name: "enroll", //注册
      component: enroll
    },
    {
      path: "*",
      redirect: '/index'
    }
  ]
});
